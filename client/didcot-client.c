/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <glib-unix.h>
#include <glib/gi18n.h>
#include <gio/gio.h>

#include "didcot.h"

/* Exit statuses. */
typedef enum
{
  /* Success. */
  EXIT_OK = 0,
  /* Error parsing command line options. */
  EXIT_INVALID_OPTIONS = 1,
  /* Bus or well-known name unavailable. */
  EXIT_BUS_UNAVAILABLE = 2,
  /* Command failed. */
  EXIT_FAILED = 3,
} ExitStatus;

/* Commands. @args is the remainder of the command line arguments following
 * the command name (which is removed). @task is a #GTask which the command
 * function must call g_task_return_*() on once complete. */
typedef void
(*CommandFunc) (GObject *object, const gchar * const *args, GTask *task);

typedef struct
{
  const gchar *name;
  CommandFunc func;
  const gchar *description;
} CommandData;

static gchar *
list_available_commands (const CommandData *commands, gsize n_commands)
{
  GString *out = NULL;
  gsize i;
  gint max_width = 0;

  /* Work out the width of the longest command name. */
  for (i = 0; i < n_commands; i++)
    max_width = MAX (max_width, (gint) strlen (commands[i].name));

  /* Build the output. */
  out = g_string_new ("");
  g_string_append_printf (out, "%s\n", "Commands:");

  for (i = 0; i < n_commands; i++)
    {
      g_string_append_printf (out, "  %*s  %s\n", max_width, commands[i].name,
                              gettext (commands[i].description));
    }

  return g_string_free (out, FALSE);
}

static void
open_uri_cb (GObject *object, GAsyncResult *res, gpointer user_data)
{
  GError *error = NULL;
  GTask *task = user_data;
  DidcotLaunch *launch_proxy = (DidcotLaunch *) object;

  if (!didcot_launch_call_open_uri_finish (launch_proxy, res, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_print ("open uri success \n");
  g_task_return_boolean (task, TRUE);
}

/* Command implementations. */
/* open-uri */
static void
open_uri (GObject *object, const gchar * const *args, GTask *task)
{
  DidcotLaunch *launch_proxy = (DidcotLaunch *) object;

  if (g_strv_length ((gchar **) args) != 1)
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                               "Missing URI ");
      return;
    }

  didcot_launch_call_open_uri (launch_proxy, args[0], NULL, open_uri_cb, task);
}

gint
main (gint argc, gchar *argv[])
{
  g_autoptr (GOptionContext) context = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GDBusConnection) connection = NULL;
  g_autoptr (GCancellable) cancellable = NULL;
  gsize i;
  g_autofree gchar *commands_help = NULL;
  g_autoptr (GTask) command_task = NULL;
  DidcotLaunch *launch_proxy = NULL;
  g_auto (GStrv) args = NULL;

  const GOptionEntry entries[] =
    {
      { G_OPTION_REMAINING, 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING_ARRAY,
          &args, NULL, NULL },
      { NULL, },
    };

  const CommandData commands[] =
    {
      { "open-uri", open_uri, "Content handover to another app/process" },
    };

  context = g_option_context_new (_ ("COMMAND [ARGS…]"));
  g_option_context_set_summary (context, "Query for Didcot service");
  g_option_context_add_main_entries (context, entries, "didcot");

  commands_help = list_available_commands (commands, G_N_ELEMENTS (commands));
  g_option_context_set_description (context, commands_help);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_autofree gchar *message;

      message = g_strdup_printf (_ ("Option parsing failed: %s"),
                                 error->message);
      g_printerr ("%s: %s", argv[0], message);

      return EXIT_INVALID_OPTIONS;
    }

  connection = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);
  if (error != NULL)
    {
      g_autofree gchar *message;

      message = g_strdup_printf ("could not able to connect to session bus %s:",
                                 error->message);
      g_printerr ("%s:%s", argv[0], message);

      return EXIT_FAILED;
    }
  launch_proxy = didcot_launch_proxy_new_sync (connection,
                                               G_DBUS_PROXY_FLAGS_NONE,
                                               "org.apertis.Didcot",
                                               "/org/apertis/Didcot/Launch",
                                               NULL, &error);

  if (error != NULL)
    {
      g_autofree gchar *message;

      message = g_strdup_printf ("could not able to get proxy %s:",
                                 error->message);
      g_printerr ("%s:%s", argv[0], message);

      return EXIT_FAILED;
    }

  if (args == NULL || args[0] == NULL)
    {
      g_printerr ("%s: %s\n\n%s", argv[0], "A command must be specified.",
                  commands_help);

      return EXIT_INVALID_OPTIONS;
    }

  for (i = 0; i < G_N_ELEMENTS (commands); i++)
    {
      if ((g_strcmp0 (commands[i].name, args[0]) == 0))
        {
          /* Run the command asynchronously. The command will run asynchronously
           * in the main context iteration below, until it calls
           * g_task_return_*() on the command_task, at which point it will
           * return. */
          if (i == 0)
            {
              command_task = g_task_new (launch_proxy, NULL, NULL, NULL);
              commands[i].func (G_OBJECT (launch_proxy),
                                (const gchar * const *) args + 1, command_task);
            }
          break;
        }
    }

  if (i == G_N_ELEMENTS (commands))
    {
      g_autofree gchar *message = NULL;

      message = g_strdup_printf ("Unrecognized command ‘%s’.", args[0]);
      g_printerr ("%s: %s\n\n%s", argv[0], message, commands_help);

      return EXIT_INVALID_OPTIONS;
    }

  /* Run the main loop until we are signalled or the command finishes. */
  while (!g_task_get_completed (command_task))
    g_main_context_iteration (NULL, TRUE);

  if (g_task_get_completed (command_task))
    {
      /* Handle errors from the command. */
      g_task_propagate_boolean (command_task, &error);

      if (error != NULL)
        {
          g_printerr ("%s: %s", argv[0], error->message);

          return EXIT_FAILED;
        }
    }

  return EXIT_OK;

}
