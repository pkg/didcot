didcot (0.2024.4) apertis; urgency=medium

  * Refresh the automatically detected licensing information

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 27 Feb 2025 13:58:35 +0000

didcot (0.2024.3) apertis; urgency=medium

  * Drop debian/source/apertis-component, moved to debian/apertis/component
  * Fix syntax of debian/copyright

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Tue, 29 Aug 2023 14:01:32 +0200

didcot (0.2024.2) apertis; urgency=medium

  * linian fix: copyright file and control file updated
  * Disabling pop-up mechanism
  * Using g_app_info API's applist creation
  * application launching using g_app_info API's

 -- Sivakrishnaprasad <sivakrishnaprasad.chinthalapudi@in.bosch.com>  Mon, 24 Jul 2023 04:52:32 +0000

didcot (0.2024.1) apertis; urgency=medium

  * Update debian/apertis/copyright
  * Add missing license information to help
    license scan to generate a complete report.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Wed, 14 Jun 2023 11:37:05 +0530

didcot (0.2024.0co1) apertis; urgency=medium

  * doc: Update documentation and build it with GTK-doc

 -- Pulluri Shirija <Pulluri.Shirija@in.bosch.com>  Fri, 16 Feb 2023 18:15:03 +0530

didcot (0.2022.1) apertis; urgency=medium

  * Add debian/apertis/lintian
  * debian/control: fix packages description
  * Switch from deprecated extra priority to optional
  * Override no-manual-page lintian tags
  * didcot-tests: override file-in-unusual-dir and non-standard-dir-in-usr
     lintian tags

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Wed, 25 Jan 2023 16:43:28 +0100

didcot (0.2022.0co2) apertis; urgency=medium

  * Remove debian/apertis/gitlab-ci.yml
  * Add debian/apertis/copyright

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Fri, 18 Mar 2022 13:44:51 +0100

didcot (0.2022.0co1) apertis; urgency=medium

  * Didcot drop legacy appfw hooks 

 -- Akshay  <M.Akshay@inbosch.com>  Mon, 07 Feb 2022 12:30:12 +0000

didcot (0.2020.2) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to target

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Wed, 23 Dec 2020 12:58:45 +0800

didcot (0.2020.1) apertis; urgency=medium

  * Switch to native format to work with the GitLab-to-OBS pipeline
  * gitlab-ci: Link to the Apertis GitLab CI pipeline definition

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Mon, 13 Jan 2020 19:17:01 +0800

didcot (0.1703.7-0co4) apertis; urgency=medium

  * debian/control: Depend on the hotdoc-0.8 branch

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 08 Jan 2020 16:02:06 +0000

didcot (0.1703.7-0co3) apertis; urgency=medium

  * debian/control: add dh-apparmor as build-deps.
  * debian/rules: call dh_apparmor for each profile that we ship in the
    binary packages.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Thu, 31 Oct 2019 14:55:11 +0800

didcot (0.1703.7-0co2) apertis; urgency=medium

  [ Simon McVittie ]
  * AppArmor: Allow logging to the Journal

  [ Emanuele Aina ]
  * debian/gbp.conf: Fix spurious escape character

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 28 Aug 2019 07:01:00 +0000

didcot (0.1703.7-0co1) 17.03; urgency=medium

  [ Rohan Madgula ]
  * Ensure all the method requests are handled by didcot (Apertis: T3757)

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Tue, 21 Mar 2017 06:46:18 +0000

didcot (0.1703.6-0co1) 17.03; urgency=medium

  [ Rohan Madgula ]
  * Display the application name instead of application id in popup
    (Apertis: T3723)
  * Display the icons of applications in selection popup (Apertis: T3723)
  * Connect to popup status signal to handle app launch cancellation
    (Apertis: T3725)

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Wed, 15 Mar 2017 16:34:21 +0000

didcot (0.1703.5-0co1) 17.03; urgency=medium

  [ Rohan Madgula ]
  * Added missing parameter in 'confirmation-result' callback of barkway
    (Apertis: T3704)
  * Maintain app_mime_list under OpenURIRequestData instead of global
    (Apertis: T3704)

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Fri, 10 Mar 2017 10:59:26 +0000

didcot (0.1703.4-0co1) 17.03; urgency=medium

  [ Emanuele Aina ]
  * didcot.desktop: Be more compliant with the bundle-spec (Apertis: T2702)

  [ Frédéric Dalleau ]
  * Remove obsolete occurence of X-ApertisManifestUrl (Apertis: T2702)

  [ Rohan Madgula ]
  * tests: Ship a test URI scheme handler (Apertis: T2702)

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Mon, 06 Mar 2017 12:59:04 +0000

didcot (0.1703.3-0co1) 17.03; urgency=medium

  [ Rohan Madgula ]
  * Corrected the signal handler id assignment (Apertis: T3674)

 -- Justin Kim <justin.kim@collabora.com>  Fri, 03 Mar 2017 18:34:20 +0900

didcot (0.1703.2-0co1) 17.03; urgency=medium

  * Remove unwanted inclusions of "canterbury.h"
  * Remove unnecessary dbus calls to canterbury
  * Use cby_entry_point_open_uri_async () to open uri (Apertis: T3572)

 -- Rohan Madgula  <Rohan.Madgula@in.bosch.com>  Mon, 13 Feb 2017 09:16:28 +0000 

didcot (0.1703.1-0co2) 17.03; urgency=medium

  [ Frédéric Dalleau ]
  * Use canterbury platform AppArmor abstraction (Apertis: T3448)

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Wed, 08 Feb 2017 09:09:22 +0000

didcot (0.1703.1-0co1) 17.03; urgency=medium

  [ Frédéric Dalleau ]
  * Port Didcot to canterbury platform (Apertis: T3448)

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Mon, 06 Feb 2017 11:26:08 +0000

didcot (0.1703.0-0co2) 17.03; urgency=medium

  [ Rohan Madgula ]
  * Added debian/tests for running the didcot tests post build.

  [ Simon McVittie ]
  * Move to debhelper compat level 10
  * Normalize packaging (wrap-and-sort -abst)
  * Produce autogenerated -dbgsym packages, not manual -dbg package

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Tue, 24 Jan 2017 10:01:22 +0000

didcot (0.1703.0-0co1) 17.03; urgency=medium

  [ Rohan Madgula ]
  * Remove non-functional "org.apertis.Didcot.Share" interface
    (Apertis: T3201)

  [ Simon McVittie ]
  * debian/gbp.conf: copy Apertis build configuration from Canterbury
  * Bump SONAME for ABI break
  * Populate symbols file
  * debian/.gitignore: add

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Fri, 20 Jan 2017 11:31:21 +0000

didcot (0.1612.0-0co1) 16.12; urgency=medium

  [ Simon McVittie ]
  * Add debian/source/apertis-component marking this as a target package

  [ Rohan Madgula ]
  * Ensure didcot API's are not exposed till canterbury dbus proxies are up
  * Set libexecdir to /usr/lib

  [ Mathieu Duponchelle ]
  * Launch.xml: Fix warnings
  * Require hotdoc version 0.8
  * docs/reference/Makefile.am: use --gdbus-codegen-sources.

  [ Marc Ordinas i Llopis ]
  * didcot-test: Don't immediately abort when assertion failed

 -- Andrew Shadura <andrew.shadura@collabora.co.uk>  Mon, 19 Dec 2016 12:09:33 +0100

didcot (0.3.2-0co1) 16.09; urgency=medium

  [ Mathieu Duponchelle ]
  * Documentation: stop using gtk-doc
  * debian: Remove doc-base file.
  * git.mk: update to upstream master
  * configure.ac: enable silent rules
  * documentation: port to hotdoc 0.8

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Thu, 15 Sep 2016 22:40:24 +0200

didcot (0.3.1-0co1) 16.09; urgency=medium

  [ Rohan Madgula ]
  * Update Didcot interface (Apertis: T2234)
    - Added didcot sample client
    - Integrated TAP test driver for installed test support
    - org.apertis.Didcot.Share interface updated
    - OpenURI () API refactored to handle multiple requests

 -- Justin Kim <joykim@collabora.com>  Fri, 26 Aug 2016 14:56:32 +0900

didcot (0.3.0-0co1) 16.06; urgency=medium

  * Rename didcot-dev to libdidcot-0-dev
  * split out a shared object into libdidcot-0 package
  * interface: merge dbus interfaces into a library
  * Update packages for support multiarch support
  * add major number to header installation directory
  * debian: add gobject-introspection data package (Apertis: T1033)
  * build: check ENABLE_GTK_DOC in docs
  * debian/control: normalized
  * add introspection support (barkway >= 0.3.0 requires)
  * scripts: remove gsettings_SCHEMAS from DISTCLEANFILES
  * didcot-popup-handler: remove an unused variable
  * didcot-launch-mgr: fix 'ISO C90 forbids mixed declarations and code' warnings
  * didcot-test: fix 'ISO C90 forbids mixed declarations and code' warnings
  * didcot: fix 'ISO C90 forbids mixed declarations and code' warnings
  * Use (void) where there are no parameters
  * Use AX_COMPILER_FLAGS instead of our own version
  * build: introduce nano version

 -- Justin Kim <justin.kim@collabora.com>  Wed, 27 Apr 2016 14:17:08 +0900

didcot (0.2.2-0co1) 16.03; urgency=medium

  [ Aleksander Morgado ]
  * build: rebuild gtk-doc sections always

  [ Sjoerd Simons ]
  * Use the generic GNOME autogen.sh
  * scripts: Add gsettings_SCHEMAS to EXTRA_DIST
  * debian: Fix copyright file

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Mon, 29 Feb 2016 21:13:11 +0100

didcot (0.2.1-0co1) 16.03; urgency=medium

  [ Simon McVittie ]
  * Clean up build system so distcheck succeeds (T770):
    - Include built headers from $(top_builddir), fixing out-of-tree build
    - Use usual name for systemduserunitdir, and put it in the ${prefix}
    - Clean up files that were generated in "make all" in "make clean"
    - Makefile.am: don't explicitly distribute files that are distributed anyway
    - Makefile.am: don't explicitly clean files that are cleaned anyway
    - Makefile.am: only clean gtk-doc.make in "make maintainer-clean"
    - Use a static README file and the standard Autotools INSTALL
    - Remove commented-out gtkdocize hack
  * Clean up Debian packaging so we can build from git (T770):
    - debian/source/options: ignore git.mk and .arcconfig in the Debian
      diff, since these are not intended to appear in tarballs but may
      appear in git; also ignore ChangeLog, which is a generated file
    - debian/rules: use autogen.sh so we can build directly from git
    - Fail the build if anything except *.la is built but not installed
    - Remove undesired dh_shlibdeps override
    - debian/docs: don't explicitly install NEWS
  * Replace entry-point schema with a desktop entry file (T571)
  * autogen.sh: don't set special CFLAGS, CXXFLAGS (T823)
  * Add a basic AppArmor profile so Canterbury can launch Didcot
    (Apertis: #616; T802)

  [ Philip Withnall ]
  * build: Add .arcconfig file

  [ Guillaume Desmottes ]
  * arcconfig: add 'projects' and 'default-reviewers' fields

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Thu, 07 Jan 2016 15:05:10 +0000

didcot (0.2.0-0rb2) 15.03; urgency=low

  * Initial import

 -- Apertis packagers <packagers@lists.apertis.org>  Thu, 12 Mar 2015 15:46:42 +0530
