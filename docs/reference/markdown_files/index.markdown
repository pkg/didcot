# Didcot

Didcot is a service responsible for sharing the data across various
applications, and it is also provides "open-with" framework where a data
can be opened with different applications which supports its mime type.

## Overview

Didcot service provides two interfaces:

### Didcot launch interface

This component is a part of didcot process. It provides interfaces to launch
applications based on mimes supported by the application to be launched.
Every application can support one or more mime types and mime types currently
supported in system are defined in "mime-types.h". This service is responsible
for displaying and launching the applications based on mime type.for example
a mp3 audio file can be launched by various music player applications
which supports same mime type.

### Didcot share interface

This D-BUS interface is responsible for sharing the data to other
applications. E.g Sharing an image through email application.

Applications supporting sharing mechanism should have data exchange
rules xml containing information about sharing and receiving
capabilities, type of information it can receive or share.

## Contact

[Mail the maintainers](mailto:maintainers@lists.apertis.org)
