/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef DIDCOT_INTERNAL_H
#define DIDCOT_INTERNAL_H

#include <glib.h>
#include <gio/gio.h>
#include <glib/gstdio.h>
#include <stdlib.h>

#define DIDCOT_DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define DIDCOT_CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define DIDCOT_WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)


#define DATA_EXCHANGE_MGR_NAME   "Data-Exchange-Manager"
#define APP_LAUNCH_MGR_NAME      "AppLaunchMgr"

void app_launch_init (void);
void app_launch_interface_initialize(GDBusConnection *connection);
void popup_client_handler_init (void);
void handle_data_exchange_mgr_confirmation_result( const gchar *title, const gchar *confirmation_result );
void handle_data_exchange_mgr_error_msg( const gchar *error_msg, gpointer user_data);

typedef struct
{
  gchar* type;
  gchar* uri;
  GList* app_list;
} MimeAppList;

typedef struct
{
  GDBusMethodInvocation *invocation;
  gchar *uri;
  gulong confirmation_result_handler_id;
  gulong error_handler_id;
  gulong popup_status_handler_id;
  MimeAppList *app_mime_list;
} OpenURIRequestData;

typedef struct
{
	/* TODO Removed barkway dependency
	 * and need to re-implement new logic
	 */

  gchar *pAppIconPath;
  GPtrArray *request_list;
} DataShareStruct;

extern DataShareStruct data_struct;

void open_uri_request_data_free (OpenURIRequestData *data);
void didcot_show_selection_popup_for_request (OpenURIRequestData *request);
void launch_app_with_url (const gchar *file_path, const gchar *mimetype, const gchar *app_name, OpenURIRequestData *request_data);

/* TODO Removed barkway dependency fun didcot_get_icon_path()
 * and need to re-implement new logic
 */

#endif
