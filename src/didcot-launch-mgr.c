/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "org.apertis.Didcot.Launch.h"
#include "didcot-internal.h"

DataShareStruct data_struct = { NULL };

static void mime_app_list_free (MimeAppList *mime_app_list);
static void _didcot_handle_uri_scheme_mimes (OpenURIRequestData *request_data);
static void _didcot_handle_guess_content_for_mime (OpenURIRequestData *request_data);

void
open_uri_request_data_free (OpenURIRequestData *data)
{
  g_return_if_fail (data != NULL);
  g_clear_object (&data->invocation);
  g_free (data->uri);
  g_clear_pointer (&data->app_mime_list, mime_app_list_free);
  g_free (data);
}

static void
didcot_handle_url (OpenURIRequestData *request_data)
{
  DIDCOT_DEBUG ("uri %s", request_data->uri);
  if (g_str_has_prefix (request_data->uri, "file://"))
    _didcot_handle_guess_content_for_mime (request_data);
  else
    _didcot_handle_uri_scheme_mimes (request_data);
}

static void
_didcot_build_app_launch_db (OpenURIRequestData *request_data)
{
  DIDCOT_DEBUG ("enter");
  didcot_handle_url (request_data);
}

/* TODO: user_selected_idx: to be read as a user input (TODO: user selection mechanism to implemented)
 * The current implementation is defaulting to launch first application from the app_list */
static GAppInfo *
select_application_from_list_of_applications(GList *app_list, gint user_selected_idx)
{
  GList *apps_list = NULL;
  GAppInfo *app_info  = NULL;
  gint app_idx = 0;

  for (apps_list = app_list, app_idx = 0; apps_list != NULL; apps_list = apps_list->next, app_idx++)
  {
    app_info = apps_list->data;

    if (app_idx == user_selected_idx)
    {
      DIDCOT_DEBUG ("%d: %s\n", app_idx, g_app_info_get_id (app_info));
      return app_info;
    }
  }
  return NULL;
}

static void
launch_application (OpenURIRequestData *request_data, GAppInfo *app_info)
{
  g_autoptr (GError) error = NULL;
  GList *uri = g_list_alloc ();

  g_list_append (uri, request_data->app_mime_list->uri);

  if (g_app_info_launch_uris (app_info, uri, NULL, &error) == FALSE)
  {
    g_error ("Could not launch application: [%s]\n", error->message);
  }
  else
  {
    didcot_launch_complete_open_uri (NULL, request_data->invocation);
  }
}

static void
launch_desired_app_with_url (OpenURIRequestData *request_data)
{
  MimeAppList *mime_app_list = NULL;
  g_autoptr (GError) error = NULL;
  gint apps_len = 0;

  g_assert (request_data != NULL);
  mime_app_list = request_data->app_mime_list;
  apps_len = g_list_length (mime_app_list->app_list);

  DIDCOT_DEBUG ("Number of supporting apps: %d\n", apps_len);
  if (apps_len == 1)
  {
    if (g_app_info_launch_default_for_uri (request_data->app_mime_list->uri, NULL, &error) == FALSE)
    {
      GList *l;
      for (l = request_data->app_mime_list->app_list; l != NULL; l = l->next)
      {
        DIDCOT_DEBUG("App: %s", l->data);
      }
      g_error ("Could not launch application: [%s]\n", error->message);
    }
    else
    {
      didcot_launch_complete_open_uri (NULL, request_data->invocation);
    }
  }
  else
  {
    GAppInfo *app_info = NULL;

    app_info = select_application_from_list_of_applications (mime_app_list->app_list, 0);

    if (app_info != NULL)
    {
      launch_application (request_data, app_info);
    }
    else
    {
      g_dbus_method_invocation_return_error (request_data->invocation,
		      G_DBUS_ERROR,
		      G_DBUS_ERROR_UNKNOWN_METHOD,
		      "User selection out of range\n");
    }
  }
}

static void
mime_app_list_free (MimeAppList *mime_app_list)
{
  g_return_if_fail (mime_app_list != NULL);
  g_free (mime_app_list->type);
  g_free (mime_app_list->uri);
  g_list_free_full (mime_app_list->app_list, g_object_unref);
  g_free (mime_app_list);
}

static void
_didcot_handle_guess_content_for_mime (OpenURIRequestData *request_data)
{
  gchar* file_mime = g_content_type_guess (request_data->uri, NULL, 0, NULL);

  DIDCOT_DEBUG ("%p %s", request_data, request_data->uri);

  if (g_content_type_is_unknown (file_mime))
    {

      g_dbus_method_invocation_return_dbus_error (
          request_data->invocation,
          "org.apertis.Didcot.Launch.Error",
          "Unable to detect the mime type");
      g_free (file_mime);
      open_uri_request_data_free (request_data);
      return;
    }
  request_data->app_mime_list = g_new0 (MimeAppList, 1);
  request_data->app_mime_list->type = g_strdup (file_mime);
  request_data->app_mime_list->uri = g_strdup (request_data->uri);
  request_data->app_mime_list->app_list = g_app_info_get_all_for_type (request_data->app_mime_list->type);
  g_free (file_mime);
  if (request_data->app_mime_list->app_list == NULL)
    {
      g_dbus_method_invocation_return_dbus_error (
          request_data->invocation, "org.apertis.Didcot.Error.NoApplicationFound",
          "No Application Found");
      open_uri_request_data_free (request_data);
      return;
    }
  launch_desired_app_with_url (request_data);
}

static void
_didcot_handle_uri_scheme_mimes (OpenURIRequestData *request_data)
{
  gchar *uri_scheme = g_uri_parse_scheme (request_data->uri);

  request_data->app_mime_list = g_new0 (MimeAppList, 1);
  request_data->app_mime_list->type = g_strconcat ("x-scheme-handler", "/",
                                                   uri_scheme, NULL);
  request_data->app_mime_list->app_list = g_app_info_get_all_for_type (request_data->app_mime_list->type);
  request_data->app_mime_list->uri = g_strdup (request_data->uri);
  g_free (uri_scheme);
  if (request_data->app_mime_list->app_list == NULL)
    {
      g_dbus_method_invocation_return_dbus_error (
          request_data->invocation, "org.apertis.Didcot.Error.NoApplicationFound",
          "No Application Found");
      open_uri_request_data_free (request_data);
      return;
    }
  launch_desired_app_with_url (request_data);
}

static gboolean
handle_open_uri_cb (DidcotLaunch *object, GDBusMethodInvocation *invocation,
                    const gchar *uri, gpointer user_data)
{
  OpenURIRequestData *request_data;

  request_data = g_new0 (OpenURIRequestData, 1);
  request_data->invocation = g_object_ref (invocation);
  request_data->uri = g_strdup (uri);
  request_data->app_mime_list = NULL;

  _didcot_build_app_launch_db (request_data);
  return TRUE;
}

void app_launch_interface_initialize(GDBusConnection *connection)
{
 DidcotLaunch *app_launch_manager = didcot_launch_skeleton_new();

  g_signal_connect (app_launch_manager, "handle-open-uri",
                    G_CALLBACK (handle_open_uri_cb), NULL);

	  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (app_launch_manager),
	                                         connection,
	                                         "/org/apertis/Didcot/Launch",
	                                          NULL))
	  {
	    DIDCOT_WARNING("skeleton export error");
	  }


}
