/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#include "didcot-internal.h"

static void
selection_popup_shown_cb (GObject *source_object, GAsyncResult *res,
                          gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  OpenURIRequestData *request_data = user_data;

  /* TODO Removed barkway dependency and need to re-implement new logic
   */
  if (error != NULL)
    {
      g_signal_handler_disconnect (source_object,
                                   request_data->confirmation_result_handler_id);
      g_signal_handler_disconnect (source_object, request_data->error_handler_id);
      g_signal_handler_disconnect (source_object, request_data->popup_status_handler_id);
      g_dbus_method_invocation_return_gerror (request_data->invocation, error);
      open_uri_request_data_free (request_data);
    }
}

static void
handle_open_uri_request_list (void)
{
  while (data_struct.request_list->len > 0)
    {
      OpenURIRequestData *request_data = NULL;

      request_data = g_ptr_array_remove_index (data_struct.request_list, 0);
      didcot_show_selection_popup_for_request (request_data);
    }
}

static void
handle_open_uri_request_list_for_error (GError *error)
{
  g_return_if_fail (error != NULL);

  while (data_struct.request_list->len > 0)
    {
      OpenURIRequestData *request_data = NULL;

      request_data = g_ptr_array_remove_index (data_struct.request_list, 0);
      g_dbus_method_invocation_return_gerror (request_data->invocation, error);
      open_uri_request_data_free (request_data);
    }
}

static void popup_service_proxy_clb( GObject *source_object, GAsyncResult *res,gpointer user_data)
{
 GError *error=NULL;
 DIDCOT_DEBUG("enter");

 /* TODO Removed barkway dependency
  * and need to re-implement new logic
  */

  if (error)
    {
      DIDCOT_WARNING ("%s: code %d: %s", g_quark_to_string (error->domain),
                     error->code, error->message);
      handle_open_uri_request_list_for_error (error);
      g_error_free (error);
      return;
    }
  handle_open_uri_request_list ();
}

static void name_appeared (GDBusConnection *connection,
               	   	   	   const gchar     *name,
               	   	   	   const gchar     *name_owner,
               	   	   	   gpointer         user_data)
{
  DIDCOT_DEBUG("enter");

  /* TODO Removed barkway dependency
   * and need to re-implement new logic
   */
}

static void
name_vanished(GDBusConnection *connection,
               const gchar     *name,
               gpointer         user_data)
{
  g_autoptr (GError) error = NULL;

  DIDCOT_DEBUG("enter");
  error = g_dbus_error_new_for_dbus_error ("org.apertis.Didcot.Error",
                                           "popup service is not available");
  handle_open_uri_request_list_for_error (error);

  /* TODO Removed barkway dependency handle_popup_status_cb()
   * and need to re-implement new logic
   */
}
